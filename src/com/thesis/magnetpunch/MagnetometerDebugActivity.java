package com.thesis.magnetpunch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.WindowManager;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class MagnetometerDebugActivity extends ActionBarActivity {

    private TextView x_Value;
    private TextView y_Value;
    private TextView z_Value;
    private TextView resultant_value;

    private Boolean isFirstLoad;

    private SensorManager sensorManager;
    private Sensor magnetometer;

    private AudioManager audioManager;
    private SoundPool soundPool;
    private Boolean isSoundLoaded = false;
    private Boolean isSoundPlaying = false;
    private Integer soundID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_magnetometer_debug);

	getSupportActionBar().setTitle(R.string.title_activity_magnetometer_debug);

	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	x_Value = (TextView) findViewById(R.id.x_value);
	y_Value = (TextView) findViewById(R.id.y_value);
	z_Value = (TextView) findViewById(R.id.z_value);
	resultant_value = (TextView) findViewById(R.id.resultant_value);

	sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

	audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
	setVolumeControlStream(AudioManager.STREAM_MUSIC);

	isFirstLoad = true;
    }

    @Override
    protected void onResume() {
	super.onResume();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	sensorManager.registerListener(sensorEventListener, magnetometer, SensorManager.SENSOR_DELAY_GAME);

	soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
	soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {

	    @Override
	    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		isSoundLoaded = true;
	    }
	});
	soundID = soundPool.load(this, R.raw.punch, 1);
    }

    @Override
    protected void onPause() {
	super.onPause();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onStop() {
	super.onStop();
	sensorManager.unregisterListener(sensorEventListener);
	soundPool.release();
    }

    @Override
    public void onBackPressed() {
	startActivity(new Intent(this, StartScreenActivity.class));
	finish();
    }

    private SensorEventListener sensorEventListener = new SensorEventListener() {

	@Override
	public void onSensorChanged(SensorEvent event) {
	    Float x = event.values[0];
	    Float y = event.values[1];
	    Float z = event.values[2];

	    x_Value.setText(x.toString());
	    y_Value.setText(y.toString());
	    z_Value.setText(z.toString());

	    Double resultant = Math.sqrt(x * x + y * y + z * z);
	    resultant_value.setText(resultant.toString());
	    resultant_value.setTextColor(Color.BLACK);
	    if (!isFirstLoad && resultant > getResources().getInteger(R.integer.magnetometer_trigger)) {
		resultant_value.setTextColor(Color.RED);
		if (isSoundLoaded && !isSoundPlaying) {
		    isSoundPlaying = true;
		    int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		    int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		    float volume = currentVolume / (float) maxVolume;

		    soundPool.play(soundID, volume, volume, 1, 0, 1.0f);
		}
	    } else {
		if (isFirstLoad)
		    isFirstLoad = false;
		isSoundPlaying = false;
	    }
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	    // Nothing to do here...
	}
    };
}
