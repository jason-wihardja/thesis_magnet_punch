package com.thesis.magnetpunch;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.thesis.helper_classes.DrawWoundTask;
import com.thesis.helper_classes.GameUtilities;
import com.thesis.helper_classes.RoundedCornerTransformation;

@SuppressLint("InflateParams")
@SuppressWarnings("deprecation")
public class GameActivity extends ActionBarActivity {

    private Long lastTitleTapTime;

    private LinearLayout scoreLinearLayout;
    private TextView gameTitleTextView, scoreLabelTextView, scoreTextView;
    private Button selectImageButton, saveImageButton, postFBButton;
    private ImageView picture;
    private ProgressBar loadingSpinner;

    private AlertDialog chooseImageSourceAlertDialog;

    private SensorManager sensorManager;
    private Sensor magnetometer;

    private AudioManager audioManager;
    private SoundPool soundPool;
    private Boolean isSoundLoaded;
    private Boolean isSoundPlaying;
    private Integer soundID;

    public GameUtilities gameUtilities;
    private SimpleFacebook simpleFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_game);

	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	getSupportActionBar().hide();
	getSupportActionBar().setTitle(R.string.title_activity_game);

	gameUtilities = new GameUtilities(this);
	lastTitleTapTime = 0L;

	gameTitleTextView = (TextView) findViewById(R.id.gameTitleTextView);
	gameTitleTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.game_title_font_name)));
	gameTitleTextView.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Long now = System.currentTimeMillis();
		Integer doubleTapInterval = getResources().getInteger(R.integer.double_tap_interval);

		if (now - lastTitleTapTime <= doubleTapInterval) {
		    gameUtilities.flipIsDrawFaceRegion();
		    if (gameUtilities.getIsDrawFaceRegion()) {
			gameTitleTextView.setTextColor(Color.RED);
		    } else {
			gameTitleTextView.setTextColor(Color.WHITE);
		    }
		}

		lastTitleTapTime = now;
	    }
	});

	scoreLinearLayout = (LinearLayout) findViewById(R.id.scoreLinearLayout);
	scoreLinearLayout.setVisibility(View.GONE);

	scoreLabelTextView = (TextView) findViewById(R.id.scoreLabelTextView);
	scoreLabelTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.score_font_name)));
	scoreLabelTextView.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		performPunch(100d);
	    }
	});

	scoreTextView = (TextView) findViewById(R.id.scoreTextView);
	scoreTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.score_font_name)));
	scoreTextView.setText(gameUtilities.getScore().toString());

	loadingSpinner = (ProgressBar) findViewById(R.id.loadingSpinner);

	selectImageButton = (Button) findViewById(R.id.selectImageButton);
	saveImageButton = (Button) findViewById(R.id.saveImageButton);
	postFBButton = (Button) findViewById(R.id.postFacebookButton);

	selectImageButton.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View view) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GameActivity.this);
		alertDialogBuilder.setTitle(R.string.select_image_text);

		LinearLayout dialogContent = (LinearLayout) getLayoutInflater().inflate(R.layout.image_chooser_dialog,
			null);

		LinearLayout cameraLayout = (LinearLayout) dialogContent.findViewById(R.id.cameraLayout);
		cameraLayout.setOnClickListener(dialogLayoutClicked);

		LinearLayout galleryLayout = (LinearLayout) dialogContent.findViewById(R.id.galleryLayout);
		galleryLayout.setOnClickListener(dialogLayoutClicked);

		LinearLayout facebookLayout = (LinearLayout) dialogContent.findViewById(R.id.facebookLayout);
		facebookLayout.setOnClickListener(dialogLayoutClicked);

		alertDialogBuilder.setView(dialogContent);
		alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

		    @Override
		    public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
		    }
		});

		chooseImageSourceAlertDialog = alertDialogBuilder.create();
		chooseImageSourceAlertDialog.show();
	    }
	});
	selectImageButton.setVisibility(View.VISIBLE);

	picture = (ImageView) findViewById(R.id.pictureImageView);
	Picasso.with(this).load(R.drawable.default_avatar_resized)
		.resize(gameUtilities.getImageDimension(), gameUtilities.getImageDimension()).centerCrop()
		.transform(new RoundedCornerTransformation(this, gameUtilities)).into(picture, new Callback() {

		    @Override
		    public void onError() {
			loadingSpinner.setVisibility(View.GONE);
		    }

		    @Override
		    public void onSuccess() {
			loadingSpinner.setVisibility(View.GONE);
		    }
		});

	saveImageButton.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		gameUtilities.saveImageToGallery(picture, selectImageButton, saveImageButton, postFBButton);
	    }
	});
	saveImageButton.setVisibility(View.GONE);

	postFBButton.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		if (!simpleFacebook.isLogin()) {
		    simpleFacebook.login(new OnLoginListener() {

			private ProgressDialog progressDialog;

			@Override
			public void onNotAcceptingPermissions(Type type) {
			    Log.w("SIMPLE_FACEBOOK_NOT_ACCEPTING_PERMISSION", type.name());
			}

			@Override
			public void onLogin() {
			    progressDialog.dismiss();
			    Toast.makeText(GameActivity.this, getString(R.string.login_fb_done_text),
				    Toast.LENGTH_SHORT).show();
			    gameUtilities.postImageToFacebook(simpleFacebook, picture);
			}

			@Override
			public void onThinking() {
			    progressDialog = ProgressDialog.show(GameActivity.this, null,
				    getString(R.string.login_fb_loading_text), true, false);
			}

			@Override
			public void onException(Throwable throwable) {
			    progressDialog.dismiss();
			}

			@Override
			public void onFail(String reason) {
			    progressDialog.dismiss();
			}
		    });
		} else {
		    gameUtilities.postImageToFacebook(simpleFacebook, picture);
		}
	    }
	});
	postFBButton.setVisibility(View.GONE);

	chooseImageSourceAlertDialog = null;

	sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

	audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
	setVolumeControlStream(AudioManager.STREAM_MUSIC);
	isSoundLoaded = false;
	isSoundPlaying = false;
    }

    @Override
    protected void onResume() {
	super.onResume();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	simpleFacebook = SimpleFacebook.getInstance(this);

	if (chooseImageSourceAlertDialog != null) {
	    chooseImageSourceAlertDialog.dismiss();
	}

	sensorManager.registerListener(magnetometerEventListener, magnetometer, SensorManager.SENSOR_DELAY_GAME);

	soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
	soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {

	    @Override
	    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		isSoundLoaded = true;
	    }
	});
	soundID = soundPool.load(this, R.raw.punch, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentData) {
	simpleFacebook.onActivityResult(this, requestCode, resultCode, intentData);

	if (resultCode == RESULT_OK) {
	    if (requestCode == getResources().getInteger(R.integer.IMAGE_CAMERA_REQUEST_CODE)) {
		// Bitmap photo = (Bitmap) intentData.getExtras().get("data");
		// picture.setImageBitmap(photo);

		chooseImageSourceAlertDialog.dismiss();
		gameUtilities.displayError("Not yet implemented.");
	    } else if (requestCode == getResources().getInteger(R.integer.IMAGE_GALLERY_REQUEST_CODE)) {
		if (intentData != null) {
		    gameUtilities.detectFace(intentData.getData(), picture, loadingSpinner, scoreLinearLayout,
			    selectImageButton, saveImageButton, postFBButton);
		} else {
		    gameUtilities.displayError("Error loading image!");
		}
	    } else if (requestCode == getResources().getInteger(R.integer.IMAGE_FACEBOOK_REQUEST_CODE)) {
		if (intentData != null) {
		    Bundle extras = intentData.getExtras();
		    if (extras != null) {
			gameUtilities.detectFace(extras.getString("profilePictureURL"), picture, loadingSpinner,
				scoreLinearLayout, selectImageButton, saveImageButton, postFBButton);
		    } else {
			gameUtilities.displayError("Error loading image!");
		    }
		} else {
		    gameUtilities.displayError("Error loading image!");
		}
	    }

	    gameUtilities.setScore(0);
	    scoreTextView.setText("0");
	}

	super.onActivityResult(requestCode, resultCode, intentData);
    }

    @Override
    protected void onPause() {
	super.onPause();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	soundPool.release();
    }

    @Override
    protected void onStop() {
	super.onStop();
    }

    @Override
    public void onBackPressed() {
	new AlertDialog.Builder(this).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		gameUtilities.recycleCurrentImage();

		startActivity(new Intent(GameActivity.this, StartScreenActivity.class));
		finish();

		System.gc();
	    }
	}).setNegativeButton("No", new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		dialog.cancel();
	    }
	}).setTitle("Exit").setMessage("Are you sure you want to exit?").setCancelable(false).create().show();
    }

    private OnClickListener dialogLayoutClicked = new OnClickListener() {

	@Override
	public void onClick(View view) {
	    switch (view.getId()) {
	    case R.id.cameraLayout:
		// Intent cameraIntent = new
		// Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		// startActivityForResult(cameraIntent,
		// getResources().getInteger(R.integer.IMAGE_CAMERA_REQUEST_CODE));
		gameUtilities.displayError("Not yet implemented.");
		break;

	    case R.id.galleryLayout:
		Intent galleryIntent = new Intent(Intent.ACTION_PICK,
			android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galleryIntent, getResources().getInteger(R.integer.IMAGE_GALLERY_REQUEST_CODE));
		break;

	    case R.id.facebookLayout:
		// FB code
		Intent facebookIntent = new Intent(GameActivity.this, SelectFriendActivity.class);
		startActivityForResult(facebookIntent,
			getResources().getInteger(R.integer.IMAGE_FACEBOOK_REQUEST_CODE));
		break;

	    default:
		chooseImageSourceAlertDialog.dismiss();
		break;
	    }
	}
    };

    private SensorEventListener magnetometerEventListener = new SensorEventListener() {

	@Override
	public void onSensorChanged(SensorEvent event) {
	    Float x = event.values[0];
	    Float y = event.values[1];
	    Float z = event.values[2];

	    Double resultant = Math.sqrt(x * x + y * y + z * z);
	    performPunch(resultant);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	    // Nothing to do here...
	}
    };

    private void performPunch(Double power) {
	if (power > getResources().getInteger(R.integer.magnetometer_trigger)) {
	    if (isSoundLoaded && !isSoundPlaying) {
		isSoundPlaying = true;
		int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = currentVolume / (float) maxVolume;

		new DrawWoundTask(GameActivity.this, gameUtilities, picture, loadingSpinner, scoreTextView, soundPool,
			soundID, volume).execute();
	    }
	} else {
	    isSoundPlaying = false;
	}
    }
}
