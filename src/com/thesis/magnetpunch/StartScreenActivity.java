package com.thesis.magnetpunch;

import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.thesis.helper_classes.BackgroundSurfaceView;
import com.thesis.helper_classes.GameUtilities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

@SuppressWarnings({ "deprecation", "unused" })
public class StartScreenActivity extends ActionBarActivity {

    private ImageView logoImageView;
    private Button startButton, uiDemoButton, debugButton, loginFBButton, logoutFBButton;
    // private BackgroundSurfaceView backgroundSurfaceView;
    private Animation buttonAnimation;

    private SimpleFacebook simpleFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	getSupportActionBar().hide();
	setContentView(R.layout.activity_start_screen);

	GameUtilities.setupFacebookConfiguration(this);

	// backgroundSurfaceView = (BackgroundSurfaceView)
	// findViewById(R.id.backgroundSurfaceView);

	startButton = (Button) findViewById(R.id.startButton);
	uiDemoButton = (Button) findViewById(R.id.uiDemoButton);
	debugButton = (Button) findViewById(R.id.debugButton);
	loginFBButton = (Button) findViewById(R.id.loginFBButton);
	logoutFBButton = (Button) findViewById(R.id.logoutFBButton);

	buttonAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.main_button_animation);
	buttonAnimation.setAnimationListener(buttonAnimationListener);

	startButton.startAnimation(buttonAnimation);
	uiDemoButton.startAnimation(buttonAnimation);
	debugButton.startAnimation(buttonAnimation);
	loginFBButton.startAnimation(buttonAnimation);
	logoutFBButton.startAnimation(buttonAnimation);

	logoImageView = (ImageView) findViewById(R.id.logoImageView);
    }

    @Override
    protected void onResume() {
	super.onResume();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	simpleFacebook = SimpleFacebook.getInstance(this);
	if (simpleFacebook.isLogin()) {
	    loginFBButton.setVisibility(View.GONE);
	    logoutFBButton.setVisibility(View.VISIBLE);
	} else {
	    loginFBButton.setVisibility(View.VISIBLE);
	    logoutFBButton.setVisibility(View.GONE);
	}

	// backgroundSurfaceView.doResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentData) {
	simpleFacebook.onActivityResult(this, requestCode, resultCode, intentData);
	super.onActivityResult(requestCode, resultCode, intentData);
    }

    @Override
    protected void onPause() {
	super.onPause();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	// backgroundSurfaceView.doPause();
    }

    @Override
    public void onBackPressed() {
	// backgroundSurfaceView.updateCanvasThread.setRunning(false);
	finish();
    }

    private AnimationListener buttonAnimationListener = new AnimationListener() {
	@Override
	public void onAnimationStart(Animation animation) {
	    startButton.setOnClickListener(null);
	    uiDemoButton.setOnClickListener(null);
	    debugButton.setOnClickListener(null);
	    loginFBButton.setOnClickListener(null);
	    logoutFBButton.setOnClickListener(null);
	}

	@Override
	public void onAnimationEnd(Animation animation) {
	    startButton.setOnClickListener(buttonClickListener);
	    uiDemoButton.setOnClickListener(buttonClickListener);
	    debugButton.setOnClickListener(buttonClickListener);
	    loginFBButton.setOnClickListener(buttonClickListener);
	    logoutFBButton.setOnClickListener(buttonClickListener);
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	    // Nothing to do here...
	}
    };

    private OnClickListener buttonClickListener = new OnClickListener() {
	@Override
	public void onClick(View v) {
	    switch (v.getId()) {
	    case R.id.startButton:
		// backgroundSurfaceView.updateCanvasThread.setRunning(false);
		startActivity(new Intent(StartScreenActivity.this, GameActivity.class));
		finish();
		System.gc();
		break;

	    case R.id.uiDemoButton:
		// backgroundSurfaceView.updateCanvasThread.setRunning(false);
		startActivity(new Intent(StartScreenActivity.this, HCIDemoActivity.class));
		finish();
		System.gc();
		break;

	    case R.id.debugButton:
		// backgroundSurfaceView.updateCanvasThread.setRunning(false);
		startActivity(new Intent(StartScreenActivity.this, MagnetometerDebugActivity.class));
		finish();
		break;

	    case R.id.loginFBButton:
		if (!simpleFacebook.isLogin()) {
		    simpleFacebook.login(new OnLoginListener() {

			private ProgressDialog progressDialog;

			@Override
			public void onNotAcceptingPermissions(Type type) {
			    Log.w("SIMPLE_FACEBOOK_NOT_ACCEPTING_PERMISSION", type.name());
			}

			@Override
			public void onLogin() {
			    progressDialog.dismiss();
			    loginFBButton.setVisibility(View.GONE);
			    logoutFBButton.setVisibility(View.VISIBLE);
			    Toast.makeText(StartScreenActivity.this, getString(R.string.login_fb_done_text),
				    Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onThinking() {
			    progressDialog = ProgressDialog.show(StartScreenActivity.this, null,
				    getString(R.string.login_fb_loading_text), true, false);
			}

			@Override
			public void onException(Throwable throwable) {
			    progressDialog.dismiss();
			}

			@Override
			public void onFail(String reason) {
			    progressDialog.dismiss();
			}
		    });
		} else {
		    Toast.makeText(StartScreenActivity.this, getString(R.string.login_fb_already), Toast.LENGTH_SHORT)
			    .show();
		}
		break;

	    case R.id.logoutFBButton:
		if (simpleFacebook.isLogin()) {
		    simpleFacebook.logout(new OnLogoutListener() {

			private ProgressDialog progressDialog;

			@Override
			public void onThinking() {
			    progressDialog = ProgressDialog.show(StartScreenActivity.this, null,
				    getString(R.string.logout_fb_loading_text), true, false);
			}

			@Override
			public void onLogout() {
			    if (progressDialog != null)
				progressDialog.dismiss();
			    loginFBButton.setVisibility(View.VISIBLE);
			    logoutFBButton.setVisibility(View.GONE);
			    Toast.makeText(StartScreenActivity.this, getString(R.string.logout_fb_done_text),
				    Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onException(Throwable throwable) {
			    progressDialog.dismiss();
			}

			@Override
			public void onFail(String reason) {
			    progressDialog.dismiss();
			    Log.e("SIMPLE_FACEBOOK_LOGOUT", reason);
			}
		    });
		} else {
		    Toast.makeText(StartScreenActivity.this, getString(R.string.logout_fb_already), Toast.LENGTH_SHORT)
			    .show();
		}
		break;

	    default:
		onBackPressed();
		break;
	    }
	}
    };
}
