package com.thesis.magnetpunch;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

@SuppressWarnings("deprecation")
public class HCIDemoActivity extends ActionBarActivity {

    private Float lastXPos, lastYPos;
    private Integer showDebugInfoDuration;
    private static Integer threadCount;

    private Boolean isMagnetometerOn;
    private Boolean isMagnetometerInProximity;

    private LinearLayout magnetometerValuesLinearLayout;
    private TableLayout lastPositionValuesTableLayout;

    private TextView x_Value, y_Value, z_Value;
    private TextView resultant_value;
    private TextView inputIndicatorTextView, xEnterValue, xLeaveValue;

    private ImageView cityImageView, forestImageView, waterfallImageView, leavesImageView;

    private ViewFlipper viewFlipper;
    private ToggleButton magnetometerToggle;

    private SensorManager sensorManager;
    private Sensor magnetometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_hci_demo);

	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	this.isMagnetometerOn = false;
	this.isMagnetometerInProximity = false;
	HCIDemoActivity.threadCount = 0;
	this.showDebugInfoDuration = getResources().getInteger(R.integer.animation_duration_1500);

	ActionBar actionBar = getSupportActionBar();
	actionBar.setDisplayShowHomeEnabled(true);
	actionBar.setDisplayShowTitleEnabled(true);
	actionBar.setDisplayShowCustomEnabled(true);
	actionBar.setTitle(R.string.magnetometer_sensor_text);
	actionBar.setCustomView(R.layout.toggle_layout);

	magnetometerValuesLinearLayout = (LinearLayout) findViewById(R.id.magnetometerValuesLinearLayout);
	magnetometerValuesLinearLayout.setVisibility(View.GONE);

	lastPositionValuesTableLayout = (TableLayout) findViewById(R.id.lastPositionValuesTableLayout);
	lastPositionValuesTableLayout.setVisibility(View.GONE);

	x_Value = (TextView) findViewById(R.id.x_value);
	y_Value = (TextView) findViewById(R.id.y_value);
	z_Value = (TextView) findViewById(R.id.z_value);
	resultant_value = (TextView) findViewById(R.id.resultant_value);

	inputIndicatorTextView = (TextView) findViewById(R.id.inputIndicatorTextView);
	xEnterValue = (TextView) findViewById(R.id.xEnterValue);
	xLeaveValue = (TextView) findViewById(R.id.xLeaveValue);

	magnetometerToggle = (ToggleButton) getSupportActionBar().getCustomView().findViewById(R.id.magnetometerToggle);
	magnetometerToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

	    @Override
	    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		isMagnetometerOn = isChecked;

		if (isMagnetometerOn) {
		    magnetometerValuesLinearLayout.setVisibility(LinearLayout.VISIBLE);
		    sensorManager.registerListener(sensorEventListener, magnetometer, SensorManager.SENSOR_DELAY_GAME);
		} else {
		    magnetometerValuesLinearLayout.setVisibility(LinearLayout.GONE);
		    sensorManager.unregisterListener(sensorEventListener);
		}
	    }
	});

	viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

	cityImageView = (ImageView) findViewById(R.id.cityImageView);
	Picasso.with(this).load(R.drawable.city).noPlaceholder().into(cityImageView);

	forestImageView = (ImageView) findViewById(R.id.forestImageView);
	Picasso.with(this).load(R.drawable.forest).noPlaceholder().into(forestImageView);

	waterfallImageView = (ImageView) findViewById(R.id.waterfallImageView);
	Picasso.with(this).load(R.drawable.waterfall).noPlaceholder().into(waterfallImageView);

	leavesImageView = (ImageView) findViewById(R.id.leavesImageView);
	Picasso.with(this).load(R.drawable.leaves).noPlaceholder().into(leavesImageView);

	sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    @Override
    protected void onResume() {
	super.onResume();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	if (this.isMagnetometerOn) {
	    sensorManager.registerListener(sensorEventListener, magnetometer, SensorManager.SENSOR_DELAY_GAME);
	}
    }

    @Override
    protected void onPause() {
	super.onPause();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onStop() {
	super.onStop();

	if (this.isMagnetometerOn) {
	    sensorManager.unregisterListener(sensorEventListener);
	}
    }

    @Override
    public void onBackPressed() {
	startActivity(new Intent(this, StartScreenActivity.class));
	finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
	if (!isMagnetometerOn) {
	    switch (touchEvent.getAction()) {
	    case MotionEvent.ACTION_DOWN:
		this.lastXPos = touchEvent.getX();
		this.lastYPos = touchEvent.getY();
		break;

	    case MotionEvent.ACTION_UP:
		Float currentTouchX = touchEvent.getX();
		Float currentTouchY = touchEvent.getY();

		flipNextOrPrevious(lastXPos, lastYPos, currentTouchX, currentTouchY);
		break;
	    }
	}

	return super.onTouchEvent(touchEvent);
    }

    private SensorEventListener sensorEventListener = new SensorEventListener() {

	@Override
	public void onSensorChanged(SensorEvent event) {
	    Float x = event.values[0];
	    Float y = event.values[1];
	    Float z = event.values[2];

	    x_Value.setText(x.toString());
	    y_Value.setText(y.toString());
	    z_Value.setText(z.toString());

	    Double resultant = Math.sqrt(x * x + y * y + z * z);
	    resultant_value.setText(resultant.toString());

	    if (resultant > getResources().getInteger(R.integer.magnetometer_trigger)) {
		resultant_value.setTextColor(Color.RED);

		if (!isMagnetometerInProximity) {
		    isMagnetometerInProximity = true;
		    lastXPos = x;
		    lastYPos = y;
		}
	    } else {
		resultant_value.setTextColor(Color.WHITE);

		if (isMagnetometerInProximity) {
		    isMagnetometerInProximity = false;
		    flipNextOrPrevious(lastXPos, lastYPos, x, y);
		}
	    }
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	    // Nothing to do here...
	}
    };

    private void flipNextOrPrevious(Float lastX, Float lastY, Float currentX, Float currentY) {
	Float xDifference = Math.abs(currentX - lastX);
	Float yDifference = Math.abs(currentY - lastY);

	if (isMagnetometerOn || xDifference > yDifference) {
	    // Swipe left or right
	    if (currentX < lastX) {
		// Swipe left
		viewFlipper.setInAnimation(this, R.anim.slide_left_in);
		viewFlipper.setOutAnimation(this, R.anim.slide_left_out);
		viewFlipper.showNext();
	    } else {
		// Swipe right
		viewFlipper.setInAnimation(this, R.anim.slide_right_in);
		viewFlipper.setOutAnimation(this, R.anim.slide_right_out);
		viewFlipper.showPrevious();
	    }

	    new ShowXDebugValuesTask(lastPositionValuesTableLayout, xEnterValue, xLeaveValue, lastX, currentX,
		    isMagnetometerOn, inputIndicatorTextView).execute(showDebugInfoDuration);
	} else {
	    // Swipe up or down
	    if (currentY > lastY) {
		// Swipe up
	    } else {
		// Swipe down
	    }
	}
    }

    public static Integer getThreadCount() {
	return threadCount;
    }

    public static void increaseThreadCount() {
	threadCount++;
    }

    public static void decreaseThreadCount() {
	threadCount--;
    }
}

final class ShowXDebugValuesTask extends AsyncTask<Integer, Integer, Void> {

    private TableLayout lastPositionValuesTableLayout;
    private TextView inputIndicatorTextView, xEnterValue, xLeaveValue;
    private Float lastX, currentX;
    private Boolean isMagnetometerOn;

    public ShowXDebugValuesTask(TableLayout lastPositionValuesTableLayout, TextView xEnterValue, TextView xLeaveValue,
	    Float lastX, Float currentX, Boolean isMagnetometerOn, TextView inputIndicatorTextView) {
	this.lastPositionValuesTableLayout = lastPositionValuesTableLayout;
	this.xEnterValue = xEnterValue;
	this.xLeaveValue = xLeaveValue;
	this.lastX = lastX;
	this.currentX = currentX;
	this.isMagnetometerOn = isMagnetometerOn;
	this.inputIndicatorTextView = inputIndicatorTextView;
    }

    @Override
    protected void onPreExecute() {
	lastPositionValuesTableLayout.setVisibility(View.VISIBLE);
	xEnterValue.setText(lastX.toString());
	xLeaveValue.setText(currentX.toString());
	inputIndicatorTextView.setText(isMagnetometerOn ? "Magnetometer" : "Touch Screen");
    }

    @Override
    protected Void doInBackground(Integer... params) {
	try {
	    HCIDemoActivity.increaseThreadCount();
	    Thread.sleep(params[0]);
	    HCIDemoActivity.decreaseThreadCount();
	} catch (InterruptedException e) {
	    Log.e("ShowXDebugValuesTask", "InterruptedException");
	}

	return null;
    }

    @Override
    protected void onPostExecute(Void result) {
	if (HCIDemoActivity.getThreadCount() == 0) {
	    lastPositionValuesTableLayout.setVisibility(View.GONE);
	}
    }
}
