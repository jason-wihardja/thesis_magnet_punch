package com.thesis.magnetpunch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnFriendsListener;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;
import com.thesis.helper_classes.FriendsArrayAdapter;

@SuppressWarnings("deprecation")
public class SelectFriendActivity extends ActionBarActivity {

    private FriendsArrayAdapter friendsAdapter;

    private EditText searchQueryEditText;
    private ListView friendsListView;
    private ProgressBar loadingSpinner;

    private SimpleFacebook simpleFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_select_friend);

	getSupportActionBar().hide();
	getSupportActionBar().setTitle(R.string.title_activity_select_friends);

	friendsListView = (ListView) findViewById(R.id.friendsListView);
	friendsListView.setOnItemClickListener(friendLayoutClicked);

	View emptyView = new View(this);
	Float density = getResources().getDisplayMetrics().density;
	Integer viewHeight = Math.round(2.5f * density);
	emptyView.setMinimumHeight(viewHeight);

	friendsListView.addHeaderView(emptyView, null, false);
	friendsListView.addFooterView(emptyView, null, false);

	loadingSpinner = (ProgressBar) findViewById(R.id.loadingSpinner);
	loadingSpinner.setVisibility(View.GONE);

	searchQueryEditText = (EditText) findViewById(R.id.searchQueryEditText);
	searchQueryEditText.addTextChangedListener(new TextWatcher() {

	    @Override
	    public void onTextChanged(CharSequence s, int start, int before, int count) {
		friendsAdapter.getFilter().filter(s.toString());
	    }

	    @Override
	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// Nothing to do here...
	    }

	    @Override
	    public void afterTextChanged(Editable s) {
		// Nothing to do here...
	    }
	});

	friendsAdapter = new FriendsArrayAdapter(SelectFriendActivity.this, new ArrayList<Profile>());
	friendsListView.setAdapter(friendsAdapter);
    }

    @Override
    protected void onResume() {
	super.onResume();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);

	simpleFacebook = SimpleFacebook.getInstance(this);
	if (!simpleFacebook.isLogin()) {
	    simpleFacebook.login(new OnLoginListener() {

		private ProgressDialog progressDialog;

		@Override
		public void onNotAcceptingPermissions(Type type) {
		    Log.w("SIMPLE_FACEBOOK_NOT_ACCEPTING_PERMISSION", type.name());
		}

		@Override
		public void onLogin() {
		    progressDialog.dismiss();
		    Toast.makeText(SelectFriendActivity.this, getString(R.string.login_fb_done_text),
			    Toast.LENGTH_SHORT).show();
		    initializeFriendList();
		}

		@Override
		public void onThinking() {
		    progressDialog = ProgressDialog.show(SelectFriendActivity.this, null,
			    getString(R.string.login_fb_loading_text), true, false);
		}

		@Override
		public void onException(Throwable throwable) {
		    progressDialog.dismiss();
		}

		@Override
		public void onFail(String reason) {
		    progressDialog.dismiss();
		}
	    });
	} else {
	    initializeFriendList();
	}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentData) {
	simpleFacebook.onActivityResult(this, requestCode, resultCode, intentData);
	super.onActivityResult(requestCode, resultCode, intentData);
    }

    @Override
    protected void onPause() {
	super.onPause();
	overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
	System.gc();
	finish();
    }

    private OnItemClickListener friendLayoutClicked = new OnItemClickListener() {

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	    String profilePictureURL = ((TextView) view.findViewById(R.id.profilePictureURL)).getText().toString();

	    Intent intent = new Intent();
	    intent.putExtra("profilePictureURL", profilePictureURL);
	    setResult(RESULT_OK, intent);
	    System.gc();
	    finish();
	}
    };

    private void initializeFriendList() {
	Integer pictureSize = getResources().getInteger(R.integer.image_dimension);
	PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
	pictureAttributes.setWidth(pictureSize);
	pictureAttributes.setHeight(pictureSize);
	pictureAttributes.setType(PictureType.SQUARE);

	Properties.Builder profilePropertiesBuilder = new Properties.Builder();
	profilePropertiesBuilder.add(Properties.ID).add(Properties.NAME).add(Properties.PICTURE, pictureAttributes);

	simpleFacebook.getTaggableFriends(profilePropertiesBuilder.build(), friendsListener);
    }

    private OnFriendsListener friendsListener = new OnFriendsListener() {

	@Override
	public void onThinking() {
	    loadingSpinner.setVisibility(View.VISIBLE);
	}

	@Override
	public void onComplete(List<Profile> response) {
	    loadingSpinner.setVisibility(View.GONE);

	    Collections.sort(response, new Comparator<Profile>() {

		@Override
		public int compare(Profile lhs, Profile rhs) {
		    return lhs.getName().compareTo(rhs.getName());
		}
	    });

	    friendsAdapter.clear();
	    friendsAdapter.addAll(response);
	    friendsAdapter.notifyDataSetChanged();
	}
    };
}