package com.thesis.helper_classes;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

@SuppressLint("WrongCall")
public class BackgroundSurfaceViewThread extends Thread {
    private Integer interval;

    private BackgroundSurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;

    private Boolean isThreadRunning;

    public BackgroundSurfaceViewThread(BackgroundSurfaceView surfaceView, SurfaceHolder surfaceHolder,
	    Integer interval) {
	this.surfaceView = surfaceView;
	this.surfaceHolder = surfaceHolder;
	this.interval = interval;
	this.isThreadRunning = false;
    }

    public void setRunning(Boolean isRunning) {
	this.isThreadRunning = isRunning;
    }

    @Override
    public void run() {
	while (isThreadRunning) {
	    Canvas canvas = null;

	    try {
		canvas = surfaceHolder.lockCanvas(null);
		synchronized (surfaceHolder) {
		    surfaceView.onDraw(canvas);
		}
		sleep(interval);
	    } catch (InterruptedException e) {
		Log.e("THREAD_RUN", e.getMessage());
	    } finally {
		if (canvas != null) {
		    surfaceHolder.unlockCanvasAndPost(canvas);
		}
	    }
	}
    }
}
