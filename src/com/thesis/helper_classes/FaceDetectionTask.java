package com.thesis.helper_classes;

import java.io.IOException;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class FaceDetectionTask extends AsyncTask<Uri, Integer, FaceRegion> {

    private Context context;
    private GameUtilities gameUtilities;

    private ImageView imageView;
    private ProgressBar loadingSpinner;
    private LinearLayout scoreLinearLayout;
    private Button selectImageButton, saveImageButton, postFBButton;

    public FaceDetectionTask(Context context, GameUtilities gameUtilities, ImageView imageView,
	    ProgressBar loadingSpinner, LinearLayout scoreLinearLayout, Button selectImageButton,
	    Button saveImageButton, Button postFBButton) {
	this.context = context;
	this.gameUtilities = gameUtilities;
	this.imageView = imageView;
	this.loadingSpinner = loadingSpinner;
	this.scoreLinearLayout = scoreLinearLayout;
	this.selectImageButton = selectImageButton;
	this.saveImageButton = saveImageButton;
	this.postFBButton = postFBButton;
    }

    @Override
    protected void onPreExecute() {
	loadingSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    protected FaceRegion doInBackground(Uri... params) {
	Bitmap faceImage = null;
	try {
	    faceImage = Picasso.with(context).load(params[0])
		    .resize(gameUtilities.getImageDimension(), gameUtilities.getImageDimension()).centerCrop().get();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	gameUtilities.setCurrentImage(faceImage);
	return gameUtilities.getFaceRegionFromCurrentImage();
    }

    @Override
    protected void onPostExecute(FaceRegion result) {
	gameUtilities.setFaceRegion(result);

	new DisplayRoundedImageTask(gameUtilities, imageView, loadingSpinner, true).execute();

	if (result.statusMessage == "OK") {
	    scoreLinearLayout.setVisibility(View.VISIBLE);
	    selectImageButton.setVisibility(View.GONE);
	    saveImageButton.setVisibility(View.VISIBLE);
	    postFBButton.setVisibility(View.VISIBLE);
	} else {
	    scoreLinearLayout.setVisibility(View.GONE);
	    Toast.makeText(context, result.statusMessage, Toast.LENGTH_SHORT).show();
	}
    }
}