package com.thesis.helper_classes;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class DisplayRoundedImageTask extends AsyncTask<Void, Integer, Bitmap> {

    private GameUtilities gameUtilities;

    private ImageView pictureContainer;
    private ProgressBar loadingSpinner;
    private Boolean showLoading;

    public DisplayRoundedImageTask(GameUtilities gameUtilities, ImageView pictureContainer, ProgressBar loadingSpinner,
	    Boolean showLoading) {
	this.gameUtilities = gameUtilities;
	this.pictureContainer = pictureContainer;
	this.loadingSpinner = loadingSpinner;
	this.showLoading = showLoading;
    }

    @Override
    protected void onPreExecute() {
	if (showLoading)
	    loadingSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
	if (gameUtilities.getIsDrawFaceRegion()) {
	    Bitmap markedImage = gameUtilities.getMarkedImage(gameUtilities.getCurrentImage(),
		    gameUtilities.getFaceRegion());
	    Bitmap roundedImage = gameUtilities.generateRoundedCorner(markedImage);

	    markedImage.recycle();

	    return roundedImage;
	} else {
	    return gameUtilities.generateRoundedCorner(gameUtilities.getCurrentImage());
	}
    }

    @Override
    protected void onPostExecute(Bitmap result) {
	pictureContainer.setImageBitmap(result);
	if (showLoading)
	    loadingSpinner.setVisibility(View.GONE);
    }
}