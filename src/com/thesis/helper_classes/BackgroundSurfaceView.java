package com.thesis.helper_classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.thesis.magnetpunch.R;

public class BackgroundSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    // Canvas Refresh Rate //
    private Integer interval = 50;
    public BackgroundSurfaceViewThread updateCanvasThread;

    private Bitmap magnetImage;
    private Integer offset, BGImageSize = 200, moveDistance = 5;

    public BackgroundSurfaceView(Context context, AttributeSet attributeSet) {
	super(context, attributeSet);
	initializeSurfaceView(context);
    }

    private void initializeSurfaceView(Context context) {
	this.offset = 0;

	Bitmap originalImage = BitmapFactory.decodeResource(getResources(), R.drawable.magnet);
	this.magnetImage = Bitmap.createScaledBitmap(originalImage, BGImageSize, BGImageSize, false);

	getHolder().addCallback(this);
	setFocusable(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
	try {
	    // Set Background Color to Light Blue
	    canvas.drawColor(getResources().getColor(R.color.bg_color_light_blue));
	    performDrawingLogic(canvas);
	    this.offset = (this.offset + this.moveDistance) % (int) Math.round(1.5 * BGImageSize);
	} catch (NullPointerException e) {
	    e.printStackTrace();
	}
    }

    private void performDrawingLogic(Canvas canvas) {
	for (Integer i = -1 * offset; i <= canvas.getWidth(); i += (int) Math.round(1.5 * BGImageSize)) {
	    for (Integer j = -1 * offset; j <= canvas.getHeight(); j += (int) Math.round(1.5 * BGImageSize)) {
		canvas.drawBitmap(magnetImage, i, j, null);
	    }
	}
    }

    public void doResume() {
	updateCanvasThread = new BackgroundSurfaceViewThread(this, getHolder(), interval);
	updateCanvasThread.setRunning(true);
	updateCanvasThread.start();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
	doResume();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	// Nothing to do here...
    }

    public void doPause() {
	Boolean retry = true;

	updateCanvasThread.setRunning(false);
	while (retry) {
	    try {
		updateCanvasThread.join();
		retry = false;
	    } catch (InterruptedException e) {
		Log.e("UPDATE_THREAD_JOIN", e.getMessage());
	    }
	}
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
	doPause();
    }
}
