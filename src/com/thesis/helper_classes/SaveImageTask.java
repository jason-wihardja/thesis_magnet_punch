package com.thesis.helper_classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.thesis.magnetpunch.R;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class SaveImageTask extends AsyncTask<ImageView, Integer, Void> {

    private Context context;
    private ProgressDialog progressDialog;
    private Button selectImageButton, saveImageButton, postFBButton;

    public SaveImageTask(Context context, Button selectImageButton, Button saveImageButton, Button postFBButton) {
	this.context = context;
	this.selectImageButton = selectImageButton;
	this.saveImageButton = saveImageButton;
	this.postFBButton = postFBButton;
    }

    @Override
    protected void onPreExecute() {
	progressDialog = ProgressDialog.show(context, null, context.getString(R.string.saving_image_text), true, false);
    }

    @Override
    protected Void doInBackground(ImageView... params) {
	Long now = System.currentTimeMillis();
	String fileName = now / 1000L + ".png";

	String path = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
	File theFolder = new File(path + "/MagnetPunch/");
	if (!theFolder.exists())
	    theFolder.mkdirs();

	try {
	    File theImageFile = new File(path + "/MagnetPunch/", fileName);
	    theImageFile.createNewFile();

	    FileOutputStream fileOutputStream = new FileOutputStream(theImageFile);

	    Bitmap image = ((BitmapDrawable) params[0].getDrawable()).getBitmap();
	    image.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);

	    fileOutputStream.flush();
	    fileOutputStream.close();

	    ContentValues contentValues = new ContentValues();
	    contentValues.put(ImageColumns.TITLE, fileName);
	    contentValues.put(ImageColumns.DESCRIPTION, fileName);
	    contentValues.put(ImageColumns.DATA, theImageFile.getAbsolutePath());
	    contentValues.put(ImageColumns.DATE_TAKEN, now);

	    context.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, contentValues);
	} catch (FileNotFoundException e) {
	    Log.e("SAVE_IMAGE", "FileNotFoundException");
	} catch (IOException e) {
	    Log.e("SAVE_IMAGE", "IOException");
	}

	return null;
    }

    @Override
    protected void onPostExecute(Void result) {
	progressDialog.dismiss();

	selectImageButton.setVisibility(View.VISIBLE);
	saveImageButton.setVisibility(View.GONE);
	postFBButton.setVisibility(View.GONE);
    }
}