package com.thesis.helper_classes;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.entities.Profile;
import com.thesis.magnetpunch.R;

@SuppressLint({ "DefaultLocale", "InflateParams" })
public class FriendsArrayAdapter extends ArrayAdapter<Profile>implements Filterable {

    private Context context;
    private Filter friendsFilter;

    private List<Profile> originalFriendsList, displayedFriendsList;

    public FriendsArrayAdapter(Context context, List<Profile> objects) {
	super(context, R.layout.friend_entry_layout, objects);
	this.context = context;
	this.friendsFilter = new FriendsFilter();
	this.originalFriendsList = objects;
	this.displayedFriendsList = objects;
    }

    @Override
    public int getCount() {
	return displayedFriendsList.size();
    }

    @Override
    public Profile getItem(int position) {
	return displayedFriendsList.get(position);
    }

    @Override
    public long getItemId(int position) {
	return getItem(position).hashCode();
    }

    private class ViewHolder {
	ImageView profilePicture;
	TextView friendID, profilePictureURL, name;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	ViewHolder viewHolder = null;
	Profile friend = getItem(position);

	LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	if (convertView == null) {
	    convertView = layoutInflater.inflate(R.layout.friend_entry_layout, null);

	    viewHolder = new ViewHolder();
	    viewHolder.friendID = (TextView) convertView.findViewById(R.id.friendID);
	    viewHolder.profilePictureURL = (TextView) convertView.findViewById(R.id.profilePictureURL);
	    viewHolder.profilePicture = (ImageView) convertView.findViewById(R.id.profilePicture);
	    viewHolder.name = (TextView) convertView.findViewById(R.id.fullName);

	    convertView.setTag(viewHolder);
	} else {
	    viewHolder = (ViewHolder) convertView.getTag();
	}

	Integer pictureSize = context.getResources().getInteger(R.integer.image_dimension);
	Picasso.with(context).load(friend.getPicture()).placeholder(R.drawable.default_avatar_resized)
		.resize(pictureSize, pictureSize).into(viewHolder.profilePicture);
	viewHolder.name.setText(friend.getName());
	viewHolder.friendID.setText(friend.getId());
	viewHolder.profilePictureURL.setText(friend.getPicture());

	return convertView;
    }

    @Override
    public Filter getFilter() {
	return this.friendsFilter;
    }

    @SuppressWarnings("unchecked")
    private class FriendsFilter extends Filter {

	@Override
	protected FilterResults performFiltering(CharSequence constraint) {
	    FilterResults filterResults = new FilterResults();

	    if (constraint == null || constraint.length() == 0) {
		filterResults.values = originalFriendsList;
		filterResults.count = originalFriendsList.size();
	    } else {
		List<Profile> constrainedFriendsList = new ArrayList<Profile>();
		for (Profile profile : originalFriendsList) {
		    if (profile.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
			constrainedFriendsList.add(profile);
		    }
		}

		filterResults.values = constrainedFriendsList;
		filterResults.count = constrainedFriendsList.size();
	    }

	    return filterResults;
	}

	@Override
	protected void publishResults(CharSequence constraint, FilterResults results) {
	    displayedFriendsList = (List<Profile>) results.values;
	    notifyDataSetChanged();
	}
    }
}