package com.thesis.helper_classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thesis.magnetpunch.R;

public class DrawWoundTask extends AsyncTask<Void, Integer, Bitmap> {

    private GameUtilities gameUtilities;

    private Context context;
    private ImageView imageView;
    private ProgressBar loadingSpinner;
    private TextView scoreTextView;

    private SoundPool soundPool;
    private Integer soundID;
    private Float volume;

    public DrawWoundTask(Context context, GameUtilities gameUtilities, ImageView imageView, ProgressBar loadingSpinner,
	    TextView scoreTextView, SoundPool soundPool, Integer soundID, Float volume) {
	this.context = context;
	this.gameUtilities = gameUtilities;
	this.imageView = imageView;
	this.loadingSpinner = loadingSpinner;
	this.scoreTextView = scoreTextView;
	this.soundPool = soundPool;
	this.soundID = soundID;
	this.volume = volume;
    }

    @Override
    protected void onPreExecute() {
	// Play Sound
	soundPool.play(soundID, volume, volume, 1, 0, 1f);
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
	FaceRegion faceRegion = gameUtilities.getFaceRegion();
	if (gameUtilities.getRandomFloat(0f, 1f) <= 0.6f && faceRegion.statusMessage.equals("OK")) {
	    // Get Wound Bitmap
	    Bitmap woundBMPFinal = gameUtilities.getRandomWoundImage().copy(Bitmap.Config.ARGB_8888, true);

	    // Calculate Wound Image Size
	    Integer faceWidth = faceRegion.bottomRight.x - faceRegion.topLeft.x;
	    Integer faceHeight = faceRegion.bottomRight.y - faceRegion.topLeft.y;

	    // Scale Wound Image
	    Float scaleFactor = gameUtilities.getRandomFloat(0.01f, 0.15f);
	    woundBMPFinal = gameUtilities.getScaledImage(woundBMPFinal,
		    faceWidth / (float) woundBMPFinal.getWidth() * scaleFactor);

	    // Rotate Wound Image
	    Float rotationAngle = gameUtilities.getRandomFloat(0f, 360f);
	    woundBMPFinal = gameUtilities.getRotatedImage(woundBMPFinal, rotationAngle);

	    // Generate Wound Position
	    PointF woundPosition = new PointF();
	    woundPosition.x = faceRegion.topLeft.x
		    + gameUtilities.getRandomFloat(0f, 0f + faceWidth - woundBMPFinal.getWidth());
	    woundPosition.y = faceRegion.topLeft.y
		    + gameUtilities.getRandomFloat(0f, 0f + faceHeight - woundBMPFinal.getHeight());

	    // Draw Wound Over Face
	    Bitmap woundedPersonImage = gameUtilities.getCurrentImage().copy(Bitmap.Config.ARGB_8888, true);
	    Canvas canvas = new Canvas(woundedPersonImage);
	    canvas.drawBitmap(woundBMPFinal, woundPosition.x, woundPosition.y, null);

	    woundBMPFinal.recycle();
	    return woundedPersonImage;
	} else {
	    return null;
	}
    }

    @Override
    protected void onPostExecute(Bitmap result) {
	// Update and display current image
	if (result != null) {
	    gameUtilities.setCurrentImage(result);
	    new DisplayRoundedImageTask(gameUtilities, imageView, loadingSpinner, false).execute();
	}

	// Increase Score
	gameUtilities.incrementScore(context.getResources().getInteger(R.integer.score_increment_value));
	scoreTextView.setText(gameUtilities.getScore().toString());
    }
}