package com.thesis.helper_classes;

import android.content.Context;
import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;
import com.thesis.magnetpunch.R;

public class RoundedCornerTransformation implements Transformation {

    private GameUtilities gameUtilities;

    private Integer cornerDips, borderDips;

    public RoundedCornerTransformation(Context context, GameUtilities gameUtilities) {
	this.gameUtilities = gameUtilities;
	this.cornerDips = context.getResources().getInteger(R.integer.rounded_corner_index);
	this.borderDips = context.getResources().getInteger(R.integer.rounded_corner_thickness);
    }

    @Override
    public String key() {
	return "RoundedCornerTransformation" + cornerDips + borderDips;
    }

    @Override
    public Bitmap transform(Bitmap source) {
	Bitmap result = gameUtilities.generateRoundedCorner(source);
	source.recycle();

	return result;
    }
}