package com.thesis.helper_classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.SessionDefaultAudience;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.listeners.OnPublishListener;
import com.thesis.magnetpunch.R;

public class GameUtilities {

    private Context context;

    public final Integer NUMBER_OF_FACES = 1;

    private Integer imageDimension;

    private Integer score;
    private Bitmap currentImage;
    private ArrayList<Bitmap> woundImages;
    private FaceRegion faceRegion;
    private Boolean isDrawFaceRegion;

    private static Random random;

    public GameUtilities(Context context) {
	this.context = context;
	isDrawFaceRegion = false;
	setScore(0);
	setImageDimension(context.getResources().getInteger(R.integer.image_dimension));
	setFaceRegion(new FaceRegion(0, 0, 0, 0, "APP_INIT"));
	random = new Random();
	new InitializeGameImagesTask().execute(context);
    }

    private class InitializeGameImagesTask extends AsyncTask<Context, Integer, Void> {

	@Override
	protected Void doInBackground(Context... params) {
	    initializeImages(params[0]);
	    return null;
	}
    }

    public void initializeImages(Context context) {
	try {
	    Integer imageDimension = context.getResources().getInteger(R.integer.image_dimension);
	    currentImage = Picasso.with(context).load(R.drawable.default_avatar_resized)
		    .resize(imageDimension, imageDimension).centerCrop().get();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	woundImages = new ArrayList<Bitmap>();
	try {
	    woundImages.add(Picasso.with(context).load(R.drawable.wound1).get());
	    woundImages.add(Picasso.with(context).load(R.drawable.wound2).get());
	    woundImages.add(Picasso.with(context).load(R.drawable.wound3).get());
	    woundImages.add(Picasso.with(context).load(R.drawable.wound4).get());
	    woundImages.add(Picasso.with(context).load(R.drawable.wound5).get());
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public static void setupFacebookConfiguration(Context context) {
	SimpleFacebook.setConfiguration(new SimpleFacebookConfiguration.Builder()
		.setPermissions(new Permission[] { Permission.PUBLISH_ACTION, Permission.READ_FRIENDLISTS,
			Permission.USER_FRIENDS, Permission.USER_PHOTOS })
		.setAppId(context.getResources().getString(R.string.facebook_app_id))
		.setNamespace(context.getResources().getString(R.string.facebook_app_id))
		.setDefaultAudience(SessionDefaultAudience.ONLY_ME).build());
    }

    public void displayError(String content) {
	new AlertDialog.Builder(context).setTitle("Error!").setIcon(R.drawable.ic_action_error).setMessage(content)
		.setPositiveButton("OK", null).create().show();
    }

    public void detectFace(Uri imageLocation, ImageView imageView, ProgressBar loadingSpinner,
	    LinearLayout scoreLinearLayout, Button selectImageButton, Button saveImageButton, Button postFBButton) {
	new FaceDetectionTask(context, this, imageView, loadingSpinner, scoreLinearLayout, selectImageButton,
		saveImageButton, postFBButton).execute(imageLocation);
    }

    public void detectFace(String imageLocation, ImageView imageView, ProgressBar loadingSpinner,
	    LinearLayout scoreLinearLayout, Button selectImageButton, Button saveImageButton, Button postFBButton) {
	detectFace(Uri.parse(imageLocation), imageView, loadingSpinner, scoreLinearLayout, selectImageButton,
		saveImageButton, postFBButton);
    }

    public FaceRegion getFaceRegionFromCurrentImage() {
	FaceDetector faceDetector = new FaceDetector(getCurrentImage().getWidth(), getCurrentImage().getHeight(),
		NUMBER_OF_FACES + 1);
	Face faces[] = new Face[NUMBER_OF_FACES + 1];

	FaceRegion faceRegion = new FaceRegion(0, 0, 0, 0, "EMPTY");

	Bitmap bitmapRGB565 = getCurrentImage().copy(Bitmap.Config.RGB_565, true);
	Integer faceCount = faceDetector.findFaces(bitmapRGB565, faces);
	if (faceCount < 1) {
	    // No face detected
	    faceRegion.statusMessage = "No face detected!";
	} else if (faceCount == 1) {
	    // Exactly 1 face detected
	    PointF faceMidPoint = new PointF();
	    faces[0].getMidPoint(faceMidPoint);

	    Float eyesDistance = faces[0].eyesDistance();
	    faceRegion.passDataAndCalculate(faceMidPoint, eyesDistance, "OK");
	} else if (faceCount > 1) {
	    // Too many faces detected
	    faceRegion.statusMessage = "More than one face detected!";
	}

	bitmapRGB565.recycle();
	return faceRegion;
    }

    public Bitmap getMarkedImage(Bitmap bitmap, FaceRegion faceRegion) {
	if (faceRegion == null) {
	    return bitmap;
	} else {
	    Bitmap result = bitmap.copy(Bitmap.Config.ARGB_8888, true);
	    Canvas canvas = new Canvas(result);

	    Paint paint = new Paint();
	    paint.setColor(Color.GREEN);
	    paint.setStyle(Style.STROKE);
	    paint.setStrokeWidth(10f);

	    Rect rect = new Rect(faceRegion.topLeft.x, faceRegion.topLeft.y, faceRegion.bottomRight.x,
		    faceRegion.bottomRight.y);
	    canvas.drawRect(rect, paint);

	    return result;
	}
    }

    public Bitmap getRotatedImage(Bitmap source, Float rotationAngle) {
	int width = source.getWidth();
	int height = source.getHeight();

	Matrix rotateMatrix = new Matrix();
	rotateMatrix.setRotate(rotationAngle, width / 2, height / 2);

	Bitmap rotatedBitmap = Bitmap.createBitmap(source, 0, 0, width, height, rotateMatrix, false);

	source.recycle();

	return rotatedBitmap;
    }

    public Bitmap getScaledImage(Bitmap source, Float scaleFactor) {
	int width = source.getWidth();
	int height = source.getHeight();

	Matrix scaleMatrix = new Matrix();
	scaleMatrix.postScale(scaleFactor, scaleFactor);

	Bitmap scaledBitmap = Bitmap.createBitmap(source, 0, 0, width, height, scaleMatrix, false);

	source.recycle();

	return scaledBitmap;
    }

    public Bitmap generateRoundedCorner(Bitmap source) {
	Integer cornerDips = context.getResources().getInteger(R.integer.rounded_corner_index);
	Integer borderDips = context.getResources().getInteger(R.integer.rounded_corner_thickness);

	Bitmap result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
	Canvas canvas = new Canvas(result);

	final int borderSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) borderDips,
		context.getResources().getDisplayMetrics());
	final int cornerSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) cornerDips,
		context.getResources().getDisplayMetrics());
	final Paint paint = new Paint();
	final Rect rect = new Rect(0, 0, source.getWidth(), source.getHeight());
	final RectF rectF = new RectF(rect);

	paint.setAntiAlias(true);
	paint.setColor(Color.WHITE);

	paint.setStyle(Style.FILL);
	canvas.drawARGB(0, 0, 0, 0);
	canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

	paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
	canvas.drawBitmap(source, rect, rect, paint);

	paint.setStyle(Style.STROKE);
	paint.setStrokeWidth((float) borderSizePx);
	canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

	return result;
    }

    public void saveImageToGallery(ImageView imageView, Button selectImageButton, Button saveImageButton,
	    Button postFBButton) {
	new SaveImageTask(context, selectImageButton, saveImageButton, postFBButton).execute(imageView);
    }

    public void postImageToFacebook(SimpleFacebook simpleFacebook, ImageView imageContainer) {
	Bitmap image = ((BitmapDrawable) imageContainer.getDrawable()).getBitmap();

	Photo photo = new Photo.Builder().setImage(image).setName(context.getString(R.string.app_name)).build();
	simpleFacebook.publish(photo, true, new OnPublishListener() {

	    @Override
	    public void onFail(String reason) {
		Toast.makeText(context, context.getString(R.string.facebook_post_fail_text), Toast.LENGTH_SHORT).show();
	    }

	    @Override
	    public void onComplete(String response) {
		Toast.makeText(context, context.getString(R.string.facebook_post_success_text), Toast.LENGTH_SHORT)
			.show();
	    }
	});
    }

    public Float getRandomFloat(Float minLimit, Float maxLimit) {
	return minLimit + (maxLimit - minLimit) * GameUtilities.random.nextFloat();
    }

    public Integer getScore() {
	return score;
    }

    public void setScore(Integer score) {
	this.score = score;
    }

    public Integer getImageDimension() {
	return imageDimension;
    }

    public void setImageDimension(Integer imageDimension) {
	this.imageDimension = imageDimension;
    }

    public void incrementScore(Integer value) {
	this.score += value;
    }

    public Bitmap getCurrentImage() {
	return currentImage;
    }

    public void setCurrentImage(Bitmap newImage) {
	if (currentImage != null && !currentImage.isRecycled()) {
	    recycleCurrentImage();
	    currentImage = null;
	}

	this.currentImage = newImage;
    }

    public void recycleCurrentImage() {
	if (currentImage != null && currentImage.isRecycled() == false) {
	    currentImage.recycle();
	}
    }

    public ArrayList<Bitmap> getWoundImages() {
	return woundImages;
    }

    public Bitmap getRandomWoundImage() {
	return woundImages.get(random.nextInt(woundImages.size()));
    }

    public FaceRegion getFaceRegion() {
	return faceRegion;
    }

    public void setFaceRegion(FaceRegion faceRegion) {
	this.faceRegion = faceRegion;
    }

    public Boolean getIsDrawFaceRegion() {
	return isDrawFaceRegion;
    }

    public void flipIsDrawFaceRegion() {
	isDrawFaceRegion = !isDrawFaceRegion;
    }
}