package com.thesis.helper_classes;

import android.graphics.Point;
import android.graphics.PointF;

public class FaceRegion {

    public String statusMessage;

    public Point topLeft;
    public Point bottomRight;

    public static final Float scaleFactor = 1.2f;
    public static final Float offsetYFactor = 0.35f;

    public FaceRegion(Integer topLeftX, Integer topLeftY, Integer bottomRightX, Integer bottomRightY,
	    String statusMessage) {
	this.statusMessage = statusMessage;

	this.topLeft = new Point(topLeftX, topLeftY);
	this.bottomRight = new Point(bottomRightX, bottomRightX);
    }

    public void passDataAndCalculate(PointF midPoint, Float eyesDistance, String statusMessage) {
	this.statusMessage = statusMessage;

	topLeft.x = Math.round(midPoint.x - eyesDistance * FaceRegion.scaleFactor);
	topLeft.y = Math.round(midPoint.y - eyesDistance * (FaceRegion.scaleFactor - FaceRegion.offsetYFactor));

	bottomRight.x = Math.round(midPoint.x + eyesDistance * FaceRegion.scaleFactor);
	bottomRight.y = Math.round(midPoint.y + eyesDistance * (FaceRegion.scaleFactor + FaceRegion.offsetYFactor));
    }
}